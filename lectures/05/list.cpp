#include "list.h"

list::list() {
	/* set list to be empty: */
	root = 0;
}

list::list(const list& L) {
	/* DONE: copy L into *this.
	 * NOTE: this is kind of tricky...  try the destructor first. */
	listNode **p = &(this->root), *pL = L.root;
	while(pL)
	{
		*p = new listNode(pL->data);
		pL = pL->next;
		p = &(*p->next);
	}
}

list::~list() {
	/* DONE: free all the nodes. */
	listNode **p = &(this->root), *doomed;
	while(*p && (doomed = *p))
	{
		p = &(*p->next);
		delete doomed;
	}
}

/* DONE: make sure the functions we wrote in the notes
 * actually work out.  (Write + test them here.) */

void list::insertAtFront(int x)
{
	listNode *newNode = new listNode;
	newNode->next = this->root;
	newNode->data = x;
	this->root = newNode;
}

bool list::eraseOne(int x)
{
	listNode *target = this->root, *guyBefore = 0;
	while(target != 0 && target->data != x){
		guyBefore = target;
		target = target->next;
	}
	if(!target) return false;
	//found it. time for step 2 & 3
	if(guyBefore)guyBefore->next = target->next;
	else //removing first node
		this->root = target->next;
	delete target;
}
