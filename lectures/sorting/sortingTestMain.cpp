/******************************************************************
Here's the main program.  We'll create a few different types of
safe arrays and test out the performance of their algorithms for
sorting integers.

Copyright (C) William E. Skeith III, 2008-2010
*********************************************************************/

#include "safeArray.h"
//we'll experiment with these in lecture:
#include "sa_heapsort.h"
#include "sa_bucketsort.h"
#include "sa_radixsort.h"
using namespace csc212;

#include <time.h>
#include <iostream>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;

void randomArray(unsigned long*& A, unsigned long size);

//preprocessor symbol for conditionally compiled code.
//if it is set, we'll test all the algorithms, otherwise
//we'll only test quick sort vs. bucket sort.
//#define testAll 1

int main()
{
	long nElts;
	clock_t start,end; //to keep track of the time
	unsigned long i; //loop index.

	srand((unsigned int) time(0)); //randomize

#ifdef testAll
	const unsigned long numArrays = 6;
#else
	const unsigned long numArrays = 3;
#endif

	safeArray* arrays[numArrays];

#ifdef testAll
	arrays[0] = new safeArray_selectSort();
	arrays[1] = new safeArray_mergeSort();
	arrays[2] = new safeArray_quickSort();
	arrays[3] = new safeArray_heapSort();
	arrays[4] = new safeArray_bucketSort();
	arrays[5] = new safeArray_radixSort();
#else
	arrays[0] = new safeArray_quickSort();
	arrays[1] = new safeArray_bucketSort();
	arrays[2] = new safeArray_radixSort();
#endif
	
	unsigned long* R; //will hold our array of random stuff to be sorted.

	while(true)
	{
		cout << "enter the number of elements, or a negative number to quit: ";
		//if the entered number is small, print out the elements of the array for testing.
		cin >> nElts;
		if(nElts<0)
			break;
		//otherwise fill the array and sort it.

		randomArray(R,nElts);
		//now make copies into each safeArray:
		for(i=0; i<numArrays; i++)
			arrays[i]->copy(R,nElts);

		//for testing, print out small arrays:
		if(nElts<16)
		{
			arrays[0]->print(nElts);
			cout << endl;
		}

		//now perform the sorting and keep track of the time.
		//note: for testing on large arrays, you'll probably want
		//to exclude selection sort from the test.
		for(i=0; i<numArrays; i++)
		{
			start = clock();
			arrays[i]->sort();
			end = clock();

			arrays[i]->printSortMethod();
			cout << " took " << (end-start)/((double)CLOCKS_PER_SEC) << " seconds.\n\n";
		}


		//for testing: print to see if each of your algorithms
		//is working properly.
		if(nElts<16)
			for(i=0; i<numArrays; i++)
			{
				arrays[i]->print(nElts);
				cout << endl;
			}

		delete[] R; //free the memory that randomArray allocated.
	}

	for(i=0; i<numArrays; i++)
		delete arrays[i];

	return 0;
}

void randomArray(unsigned long*& A, unsigned long size)
{
	//Note that RAND_MAX is a little small for some compilers (2^16-1).
	//In order to test our algorithms on large arrays without huge
	//numbers of duplicates, we'll set the high-order and low-order
	//parts of the return value with two random values.
	A = new unsigned long[size];
	for(unsigned long i=0; i<size; i++)
		A[i] = ((rand()<<16) | rand()) % 0x80000000;

	//Another note:  initially, if you want to test your program out with smaller
	//arrays and small numbers, just reduce A[i] mod k for some small value k as in the following:
	//A[i] = rand() % 16;
	//this may help you debug at first.
}