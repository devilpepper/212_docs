/*****************************************************
our implementation of the safe array class, as well as
the derived classes for sorting.
/*****************************************************/

#include<iostream>
using std::cout;
using std::cin;
using std::endl;
#include <cassert>

#include "safeArray.h"

namespace csc212
{
	/*
	here are a few prototypes for the sorting functions that will actually
	do most of the work for us.
	*/

	//selection sort functions
	void selectionSort(val_type* A, unsigned long size);
	unsigned long minIndex(val_type* A, unsigned long startIndex, unsigned long size);

	//merge sort functions
	void mergeSort(val_type* A, val_type* temp, unsigned long left, unsigned long right);
	void merge(val_type* A, val_type* temp, unsigned long left, unsigned long mid, unsigned long right);

	//quick sort functions
	void quickSort(val_type* A, unsigned long p, unsigned long r);
	unsigned long Partition(val_type* A, unsigned long p, unsigned long r);

	//generic function for swapping values in an array:
	void switch2(val_type* A, unsigned long i, unsigned long j)
	{
		val_type temp = A[i];
		A[i] = A[j];
		A[j] = temp;
	}


	////////////////implementation of the safeArray class////////////////

	safeArray::safeArray(unsigned long size)
	{
		data = new val_type[size];
		cur_size = size;
	}

	safeArray::safeArray(const safeArray& A)
	{
		cur_size = A.cur_size;
		data = new val_type[cur_size];
		for(unsigned long i=0; i<cur_size; i++)
			data[i] = A.data[i];
	}

	safeArray::~safeArray()
	{
		delete[] data;
	}

	void safeArray::resize(unsigned long size)
	{
		val_type* temp = new val_type[size];
		for(unsigned long i=0; (i<cur_size && i<size); i++)
			temp[i] = data[i];
		delete[] data;
		data = temp;
		cur_size = size;
	}

	val_type& safeArray::operator[](unsigned long n)
	{
		assert(n<cur_size); //valid index
		return data[n];
	}

	void safeArray::operator=(const safeArray& A)
	{
		delete [] data;
		cur_size = A.cur_size;
		data = new val_type[cur_size];
		for(unsigned long i=0; i<cur_size; i++)
			data[i] = A.data[i];
	}

	//not the most efficient way to do things, but simple, and it will work for us here.
	void safeArray::copy(val_type* A, unsigned long size)
	{
		this->resize(size);
		for(unsigned long i=0; i<size; i++)
			data[i] = A[i];
	}

	unsigned long safeArray::get_size()
	{
		return cur_size;
	}

	void safeArray::print(unsigned long nelts)
	{
		if(nelts == 0) //then print everything
			nelts = cur_size;
		for(unsigned long i=0; i<nelts; i++)
			cout << data[i] << " ";
	}


	
	//////////////////////now we'll implement the derived classes/////////////////


	///////////////implementation of selection sort array class//////////////
	void safeArray_selectSort::printSortMethod()
	{
		cout << "selection sort";
	}

	void safeArray_selectSort::sort(unsigned long nElts)
	{
		if(nElts == 0)
			nElts = this->cur_size;
		//call selection sort
		selectionSort(data,nElts);
	}

	///////////////implementation of merge sort array class//////////////
	void safeArray_mergeSort::printSortMethod()
	{
		cout << "merge sort";
	}

	void safeArray_mergeSort::sort(unsigned long nElts)
	{
		if(nElts == 0)
			nElts = this->cur_size;
		//call merge sort:
		val_type* temp = new val_type[this->cur_size];
		mergeSort(data,temp,0,nElts-1);
		delete[] temp;

	}

	///////////////implementation of quick sort array class//////////////
	void safeArray_quickSort::printSortMethod()
	{
		cout << "quick sort";
	}

	void safeArray_quickSort::sort(unsigned long nElts)
	{
		if(nElts == 0)
			nElts = this->cur_size;
		//just call quick sort...
		quickSort(data,0,nElts-1);
	}

	//////////here are the sorting algorithms which we'll call from our derived classes////////

	//merge sort stuff
	void mergeSort(val_type* A, val_type* temp, unsigned long left, unsigned long right)
	{
		unsigned long mid;
		if (right > left)
		{
			mid = (right + left) / 2;
			mergeSort(A, temp, left, mid);
			mergeSort(A, temp, mid+1, right);
			merge(A, temp, left, mid+1, right);
		}
	}

	void merge(val_type* A, val_type* temp, unsigned long left, unsigned long mid, unsigned long right)
	{
		//our job is to take A[left,...,mid-1] (which is sorted) and merge it with
		//A[mid,...,right] (also sorted) into the temp array.  We'll then copy it back to A.

		unsigned long left_end, tmp_pos;
		left_end = mid - 1; //this will store the last index of the left sub-array
		tmp_pos = 0; //this will index the temporary array

		//visualize the two sorted decks of cards:  just look at the top
		//of each stack, take the biggest one and place that on the top
		//of the third stack:
		while ((left <= left_end) && (mid <= right))
		{
			if (A[left] <= A[mid])
				temp[tmp_pos++] = A[left++];
			else
				temp[tmp_pos++] = A[mid++];
		}

		//now copy the contents of whichever sub-array still has something left:
		while (left <= left_end)
			temp[tmp_pos++] = A[left++];
		while (mid <= right)
			temp[tmp_pos++] = A[mid++];

		//finally, put the contents back into A:
		while(--tmp_pos != -1) //note tmp_pos is unsigned, so x>=0 equates to (true), hence the -1
			A[right--] = temp[tmp_pos];
	}

	//quick sort stuff
	unsigned long Partition(val_type* A, unsigned long p, unsigned long r)
	{
		//the basic idea is to select a partition element (we'll just use the first one)
		//and then move all smaller elements to the left and larger ones to the right,
		//finally returning a partition index
		val_type x = A[p];
		unsigned long i = p-1;
		unsigned long j = r+1;
		while(true)
		{
			do i++;
			while(A[i]<x);
			do j--;
			while(A[j]>x);

			if(i<j)
				switch2(A,i,j);
			else
				return j;
		}
		//I encourage you to think very carefully about the above code and why it works.
		//In fact, I would encourage you to attempt a proof of correctness.
		//Note that I increment i and decrement j every time through the outer loop.
		//What might go wrong if I used a while(...){...} loop instead of a do{...}while(...) ???
		//Would the algorithm still work if I returned i instead of j?  Why or why not?
		//Anyway, for a short (and non-recursive) algorithm, there is a little something to think about.
	}

	void quickSort(val_type* A, unsigned long p, unsigned long r)
	{
		unsigned long q;
		if(p<r)
		{
			q = Partition(A,p,r);
			quickSort(A,p,q);
			quickSort(A,q+1,r);
		}
	}

	//selection sort stuff
	unsigned long minIndex(val_type* A, unsigned long startIndex, unsigned long size)
	{
		unsigned long retIndex = startIndex;
		for(unsigned long i=startIndex+1; i<size; i++)
		{
			if(A[retIndex]>A[i])
				retIndex=i;
		}
		return retIndex;
	}

	void selectionSort(val_type* A, unsigned long size)
	{
		for(unsigned long i=0; i<size-1; i++)
			switch2(A,i,minIndex(A,i,size));
	}
}