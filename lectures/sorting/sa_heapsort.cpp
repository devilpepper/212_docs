#include "sa_heapsort.h"

#include <iostream>
using std::cout;

namespace csc212
{
	//heap sort functions
	//heapify, build heap, heap sort
	void heapify(val_type* A, unsigned long rootIndex, unsigned long size);
	void buildHeap(val_type* A, unsigned long size);
	void heapSort(val_type* A, unsigned long size);

	//the following functions map out our complete binary tree structure to array indexes.
	//it looks a little different in comparison to what we did in class since we would like
	//to have our array indexes start at 0.
	inline unsigned long leftChild(unsigned long i)
	{
		return 2*i+1;
	}
	inline unsigned long rightChild(unsigned long i)
	{
		return 2*i+2;
	}
	inline unsigned long parent(unsigned long i)
	{
		return (i-1)/2;
	}

	///////////////implementation of heap sort array class//////////////
	void safeArray_heapSort::printSortMethod()
	{
		cout << "heap sort";
	}

	void safeArray_heapSort::sort(unsigned long nElts)
	{
		if(nElts == 0)
			nElts = this->cur_size;
		//just call heap sort...
		heapSort(data,nElts);
	}

	//Here, we assume the rootIndex is the only node that may violate the heap property.  The left and
	//right subtrees are already heaps, so we just need to work our way down until we find 
	//out where this value goes.
	void heapify(val_type* A, unsigned long rootIndex, unsigned long size)
	{
		unsigned long maxChild = leftChild(rootIndex); //index of child with max value.
		if(maxChild >= size) return; //rootIndex is already a leaf
		if(maxChild < size-1 && A[maxChild] < A[maxChild+1])
			++maxChild;
		if(A[rootIndex] < A[maxChild]) //need to switch to preserve the heap property
		{
			switch2(A,rootIndex,maxChild);
			heapify(A,maxChild,size);
		}
		//else, there is nothing left to do as the heap property has been re-established.
		//note: if our compiler does not optimize tail-recursion, we would probably
		//want to convert this to a while loop to get rid of some function call overhead.
	}

	//here, we take an un-ordered array and create a heap, by repeatedly calling heapify.
	//This algorithm clearly runs in time O(n lg n), however, I claim it is actually linear.
	void buildHeap(val_type* A, unsigned long size)
	{
		//we'll start from the last leaf (which is a heap on its own) and then
		//call heapify on the parents, working back through the tree.
		for(unsigned long i = size/2-1; i!=-1; i--)
			heapify(A,i,size);
	}

	//idea:  turn the array into a heap.  Then the maximal element is at the root.
	//Now, swap the root with the last element, reduce the heap size, and call heapify, repeat.
	void heapSort(val_type* A, unsigned long size)
	{
		buildHeap(A,size);
		for(unsigned long i = size-1; i>0; i--)
		{
			switch2(A,0,i);
			heapify(A,0,--size);
		}
	}
}