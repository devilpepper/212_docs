#include "sa_radixsort.h"

#include <iostream>
using std::cout;

namespace csc212
{
	///////////////implementation of radix sort array class//////////////
	void countingSort(val_type* A, val_type* B, unsigned long n, unsigned long bitsOfBase, unsigned long digit);
	void radixSort(val_type* A, unsigned long size);
	void safeArray_radixSort::printSortMethod()
	{
		cout << "radix sort";
	}

	void safeArray_radixSort::sort(unsigned long nElts)
	{
		if(nElts == 0)
			nElts = this->cur_size;
		//just call radix sort...
		radixSort(data,nElts);
	}
	void radixSort(val_type* A, unsigned long size)
	{
		val_type* B = new val_type[size];
		unsigned long bits = 16; //one byte at a time: base 256
		for(unsigned long d=0; d<32/bits; d++)
		{
			countingSort(A,B,size,bits,d);
			//now copy back into A:
			for(unsigned long i=0; i<size; i++)
				A[i] = B[i];
		}
		delete[] B;
		return;
	}

	void countingSort(val_type* A, val_type* B, unsigned long n, unsigned long bitsOfBase, unsigned long digit)
	{
		unsigned long k = (1 << bitsOfBase);
		val_type mask = 0xFFFFFFFF >> (32-bitsOfBase);
		val_type* C = new val_type[k];
		unsigned long i;
		//set up our counter array C in just 3 easy steps:
		for(i=0; i<k; i++)
			C[i] = 0;
		for(i=0; i<n; i++)
			C[(A[i] >> (digit*bitsOfBase) & mask)]++;
		for(i=1; i<k; i++)
			C[i]+=C[i-1];

		//now based on the counts, place our elements into the right place in B:
		for(i=n-1; i!=-1; i--)
			B[--C[(A[i] >> (digit*bitsOfBase) & mask)]] = A[i];
		//that's really about it.
		delete[] C;
	}
}