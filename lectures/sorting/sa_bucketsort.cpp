//implementation of the bucket sort safe array.
#include "sa_bucketsort.h"

#include <iostream>
using std::cout;
using std::endl;

#include<set> //to implement the buckets (at first)
using std::set;

namespace csc212
{
	void safeArray_bucketSort::printSortMethod()
	{
		cout << "bucket sort";
	}

	void bucketSort(val_type* A, unsigned long size, unsigned long uBound);

	//we'll use the listnode structure to implement the buckets.
	struct listNode
	{
		//constructors:
		listNode()
		{
		}
		listNode(val_type x, listNode* pNext = 0)
		{
			data = x;
			next = pNext;
		}

		//data members:
		listNode* next; //pointer to the next node in the list
		val_type data; //the data for the node
	};

	//the assumption here will be that we have a collection of size integers
	//which are distributed (uniformly, we hope) in the range [0,...,uBound-1]
	void bucketSort1(val_type* A, unsigned long size, unsigned long uBound)
	{
		//first we need an array of buckets, of about size in length.
		std::set<val_type>* B = new std::set<val_type>[size];
		unsigned long bucketRange = uBound / size + 1;
		//so, for an integer k, the bucket it ends up in is k / bucketRange
		unsigned long i;
		for(i=0; i<size; i++)
			B[A[i]/bucketRange].insert(A[i]);
		//good.  now go back through the buckets and fill in the array entries.
		//we'll need a set iterator:
		std::set<val_type>::iterator ib;
		unsigned long j=0; //keep track of our place in the array
		for(i=0; i<size; i++)
		{
			for(ib=B[i].begin(); ib!=B[i].end(); ib++)
				A[j++] = *ib;
		}
		delete[] B;
	}

	void bucketSort2(val_type* A, unsigned long size, unsigned long uBound)
	{
		//first we need an array of buckets, of about size in length.
		listNode** Roots = new listNode*[size];
		unsigned long i;
		for(i=0; i<size; i++)
			Roots[i] = 0; //init to null pointer
		//temporary nodes for list manipulation:
		listNode* p;
		listNode* q = 0; //initialize this here to save time later on.
		listNode* newNode;
		unsigned long bucketRange = uBound / size + 1;
		unsigned long bIndex; //the bucket index
		//so, for an integer n, the bucket it ends up in is n / bucketRange
		for(i=0; i<size; i++)
		{
			bIndex = A[i]/bucketRange;
			p = Roots[bIndex];
			newNode = new listNode(A[i]);

			while(p && p->data < A[i])
			{
				q = p;
				p = p->next;
			}
			if(q)
			{
				q->next = newNode;
				q = 0; //reset for next time
			}
			else
				Roots[bIndex] = newNode;
			newNode->next = p;
		}
		//good.  now go back through the buckets and fill in the array entries.
		unsigned long j=0; //keep track of our place in the array
		for(i=0; i<size; i++)
		{
			p = Roots[i];
			while(p)
			{
				q = p;
				A[j++] = p->data;
				p = p->next;
				delete q; //we'll clean up the list as we go.
			}
		}
		delete[] Roots;
	}

	
	//this version will execute about the same number of instructions, but doesn't actually sort
	//the array.  Due to the high number of cache hits, this works out much faster.
	void bucketSort_NotReally(val_type* A, unsigned long size, unsigned long uBound)
	{
		//first we need an array of buckets, of about size in length.
		listNode** Roots = new listNode*[size];
		//you should allocate all of your listnodes at once too to save time:
		listNode* newNodes = new listNode[size];
		unsigned long nextNode = 0; //tells us where to get the next node from.
		unsigned long i;
		for(i=0; i<size; i++)
			Roots[i] = 0; //init to null pointer
		//temporary nodes for list manipulation:
		listNode* p;
		listNode* q = 0; //initialize this here to save time later on.
		listNode* newNode;
		unsigned long bucketRange = uBound / size + 1;
		unsigned long bIndex; //the bucket index
		//so, for an integer n, the bucket it ends up in is n / bucketRange
		for(i=0; i<size; i++)
		{
			bIndex = A[i]/bucketRange;
			bIndex = i; //i/5; //now it won't sort
			p = Roots[bIndex];
			//newNode = new listNode(A[i]);
			//replace with:
			newNode = newNodes+nextNode;
			newNode->data = A[i];
			++nextNode;

			while(p && p->data < A[i])
			{
				q = p;
				p = p->next;
			}
			if(q)
			{
				q->next = newNode;
				q = 0; //reset for next time
			}
			else
				Roots[bIndex] = newNode;
			newNode->next = p;
		}
		//good.  now go back through the buckets and fill in the array entries.
		unsigned long j=0; //keep track of our place in the array
		for(i=0; i<size; i++)
		{
			p = Roots[i];
			while(p)
			{
				//this is no longer necessary:
				//q = p;
				A[j++] = p->data;
				p = p->next;
				//this is no longer necessary:
				//delete q; //we'll clean up the list as we go.
			}
		}
		delete[] Roots;
		delete[] newNodes;
	}


	//regular version, optimized for minimal number of memory allocations
	void bucketSort0(val_type* A, unsigned long size, unsigned long uBound)
	{
		//first we need an array of buckets, of about size in length.
		listNode** Roots = new listNode*[size];
		//you should allocate all of your listnodes at once too to save time:
		listNode* newNodes = new listNode[size];
		unsigned long nextNode = 0; //tells us where to get the next node from.
		unsigned long i;
		for(i=0; i<size; i++)
			Roots[i] = 0; //init to null pointer
		//temporary nodes for list manipulation:
		listNode* p;
		listNode* q = 0; //initialize this here to save time later on.
		listNode* newNode;
		unsigned long bucketRange = uBound / size + 1;
		unsigned long bIndex; //the bucket index
		//so, for an integer n, the bucket it ends up in is n / bucketRange
		for(i=0; i<size; i++)
		{
			bIndex = A[i]/bucketRange;
			p = Roots[bIndex];
			//newNode = new listNode(A[i]);
			//replace with:
			newNode = newNodes+nextNode;
			newNode->data = A[i];
			++nextNode;

			while(p && p->data < A[i])
			{
				q = p;
				p = p->next;
			}
			if(q)
			{
				q->next = newNode;
				q = 0; //reset for next time
			}
			else
				Roots[bIndex] = newNode;
			newNode->next = p;
		}
		//good.  now go back through the buckets and fill in the array entries.
		unsigned long j=0; //keep track of our place in the array
		for(i=0; i<size; i++)
		{
			p = Roots[i];
			while(p)
			{
				//this is no longer necessary:
				//q = p;
				A[j++] = p->data;
				p = p->next;
				//this is no longer necessary:
				//delete q; //we'll clean up the list as we go.
			}
		}
		delete[] Roots;
		delete[] newNodes;
	}

	//version where we increase the number of buckets to reduce collisions:
	void bucketSort3(val_type* A, unsigned long size, unsigned long uBound)
	{
		//first we need an array of buckets, of about size in length.
		//let's try expanding the number of buckets a bit to reduce collisions.
		unsigned long newSize = size+size/2; //2*size;
		listNode** Roots = new listNode*[newSize];
		//you should allocate all of your listnodes at once too to save time:
		listNode* newNodes = new listNode[size];
		unsigned long nextNode = 0; //tells us where to get the next node from.
		unsigned long i;
		for(i=0; i<newSize; i++)
			Roots[i] = 0; //init to null pointer
		//temporary nodes for list manipulation:
		listNode* p;
		listNode* q = 0; //initialize this here to save time later on.
		listNode* newNode;
		unsigned long bucketRange = uBound / newSize + 1;
		unsigned long bIndex; //the bucket index
		//so, for an integer n, the bucket it ends up in is n / bucketRange
		for(i=0; i<size; i++)
		{
			bIndex = A[i]/bucketRange;
			p = Roots[bIndex];
			//newNode = new listNode(A[i]);
			//replace with:
			newNode = newNodes+nextNode;
			newNode->data = A[i];
			++nextNode;

			while(p && p->data < A[i])
			{
				q = p;
				p = p->next;
			}
			if(q)
			{
				q->next = newNode;
				q = 0; //reset for next time
			}
			else
				Roots[bIndex] = newNode;
			newNode->next = p;
		}
		//good.  now go back through the buckets and fill in the array entries.
		unsigned long j=0; //keep track of our place in the array
		unsigned long ccount, maxc=0;
		for(i=0; i<newSize; i++)
		{
			ccount = 0;
			p = Roots[i];
			while(p)
			{
				++ccount;
				//this is no longer necessary:
				//q = p;
				A[j++] = p->data;
				p = p->next;
				//this is no longer necessary:
				//delete q; //we'll clean up the list as we go.
			}
			if(ccount>maxc)
				maxc = ccount;
		}
		delete[] Roots;
		delete[] newNodes;
		cout << "  LONGEST LIST: " << maxc << endl;
	}


	void bucketSort(val_type* A, unsigned long size, unsigned long uBound)
	{
		unsigned long bucketRange = uBound / size + 1;
		unsigned long bIndex; //the bucket index

		//allocate our buckets.
		listNode** Roots = new listNode*[size];
		unsigned long i;
		for(i=0; i<size; i++)
			Roots[i] = 0;
		//::memset(Roots,0,sizeof(listNode*)*size);

		listNode* p;
		listNode* q;

		//dump the values into buckets...
		for(i=0; i<size; i++)
		{
			bIndex = A[i]/bucketRange;
			//now insert into Roots[bIndex]
			p = Roots[bIndex];
			q = 0;
			while(p && p->data < A[i])
			{
				q = p;
				p = p->next;
			}
			if(q) //not at the beginning of the list:
				q->next = new listNode(A[i],p);
			else
				Roots[bIndex] = new listNode(A[i],p);
		}
		//next, read back into the array...
		unsigned long j=0; //to index A
		for(i=0; i<size; i++)
		{
			p = Roots[i];
			while(p)
			{
				A[j++] = p->data;
				q = p;
				p = p->next;
				delete q;
			}
		}
		delete[] Roots;
	}
	void bucketSort4(val_type* A, unsigned long size, unsigned long uBound)
	{
		unsigned long bucketRange = uBound / size + 1;
		unsigned long bIndex; //the bucket index

		//allocate our buckets.
		listNode** Roots = new listNode*[size];
		//now, let's try to allocate all the nodes at once...
		listNode* heap_O_Nodes = new listNode[size];
		listNode* newNode = heap_O_Nodes;

		unsigned long i;
		for(i=0; i<size; i++)
			Roots[i] = 0;
		//::memset(Roots,0,sizeof(listNode*)*size);

		listNode* p;
		listNode* q;

		//dump the values into buckets...
		for(i=0; i<size; i++)
		{
			bIndex = A[i]/bucketRange;
			//now insert into Roots[bIndex]
			p = Roots[bIndex];
			q = 0;
			newNode->data = A[i];
			while(p && p->data < A[i])
			{
				q = p;
				p = p->next;
			}
			if(q) //not at the beginning of the list:
				//q->next = new listNode(A[i],p);
				q->next = newNode;
			else
				//Roots[bIndex] = new listNode(A[i],p);
				Roots[bIndex] = newNode;
			(newNode++)->next = p;
		}
		//next, read back into the array...
		unsigned long j=0; //to index A
		for(i=0; i<size; i++)
		{
			p = Roots[i];
			while(p)
			{
				A[j++] = p->data;
				p = p->next;
			}
		}
		delete[] Roots;
		delete[] heap_O_Nodes;
	}
	void bucketSort4_NotReally(val_type* A, unsigned long size, unsigned long uBound)
	{
		unsigned long bucketRange = uBound / size + 1;
		unsigned long bIndex; //the bucket index

		//allocate our buckets.
		listNode** Roots = new listNode*[size];
		//now, let's try to allocate all the nodes at once...
		listNode* heap_O_Nodes = new listNode[size];
		listNode* newNode = heap_O_Nodes;

		unsigned long i;
		for(i=0; i<size; i++)
			Roots[i] = 0;

		listNode* p;
		listNode* q;

		//dump the values into buckets...
		for(i=0; i<size; i++)
		{
			bIndex = A[i]/bucketRange;
			//Now let's overwrite the index with i/3:
			bIndex = i/3;
			//The algorithm will no longer sort properly, but it will perform a similar
			//amount of work to bucket sort in terms of # of statements executed.
			//The difference in observed running time will come from the now much 
			//improved rate that we hit the cache.  i/3 was chosen to ensure that a bit
			//of work is involved in the sorting of each list.  In fact, this algorithm would
			//probably be more expensive (slower) than bucket sort if memory speed were
			//identical to CPU speed.
			
			//now insert into Roots[bIndex]
			p = Roots[bIndex];
			q = 0;
			newNode->data = A[i];
			while(p && p->data < A[i])
			{
				q = p;
				p = p->next;
			}
			if(q) //not at the beginning of the list:
				//q->next = new listNode(A[i],p);
				q->next = newNode;
			else
				//Roots[bIndex] = new listNode(A[i],p);
				Roots[bIndex] = newNode;
			(newNode++)->next = p;
		}
		//next, read back into the array...
		unsigned long j=0; //to index A
		for(i=0; i<size; i++)
		{
			p = Roots[i];
			while(p)
			{
				A[j++] = p->data;
				p = p->next;
			}
		}
		delete[] Roots;
		delete[] heap_O_Nodes;
	}
	void safeArray_bucketSort::sort(unsigned long nElts)
	{
		if(nElts == 0)
			nElts = this->cur_size;
		//call bucket sort:
		//bucketSort(data,nElts,0x80000000);
		//bucketSort0(data,nElts,0x80000000);
		//bucketSort1(data,nElts,0x80000000);
		//bucketSort2(data,nElts,0x80000000);
		//bucketSort3(data,nElts,0x80000000);
		bucketSort4(data,nElts,0x80000000);
		//bucketSort4_NotReally(data,nElts,0x80000000);
		//bucketSort_NotReally(data,nElts,0x80000000);
		
	}
}
