//header file for safe array derivative that implements
//sorting via heap sort.

#pragma once

#include "safeArray.h"

namespace csc212
{
	class safeArray_heapSort : public safeArray
	{
		void sort(unsigned long nElts = 0);
		void printSortMethod();
	};
}