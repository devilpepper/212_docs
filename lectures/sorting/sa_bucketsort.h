//header file for safe array derivative that implements
//sorting via bucket sort.

#pragma once

#include "safeArray.h"

namespace csc212
{
	class safeArray_bucketSort : public safeArray
	{
		void sort(unsigned long nElts = 0);
		void printSortMethod();
	};
}