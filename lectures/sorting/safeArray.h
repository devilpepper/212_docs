/******************************************************************
We'll begin with our old safeArray class, then modify it to contain
abstract functions for sorting, etc.  Subsequently, we'll derive
several classes from safeArray, each of which will implement sorting
in a different way.  Finally, we'll set up some arrays and test out
the performance.

Copyright (C) William E. Skeith III, 2008-2009
*********************************************************************/

#pragma once

namespace csc212
{
	typedef unsigned long val_type;
	class safeArray
	{
	public:
		safeArray(unsigned long size=10);
		safeArray(const safeArray& A); //copy constructor
		~safeArray();
		void resize(unsigned long size);
		void print(unsigned long nelts=0);
		void copy(val_type* A, unsigned long size);
		val_type& operator[](unsigned long n);
		void operator=(const safeArray& A);
		unsigned long get_size();

		//here are our functions for sorting...
		virtual void sort(unsigned long nElts = 0) = 0;
		virtual void printSortMethod() = 0;

	protected:
		val_type* data;
		unsigned long cur_size;
	};

	
	////////////////next we'll derive classes from safeArray to sort in different ways//////////////
	//note that all of the basic functionality of the safe array
	//is inherited: we don't have to write it again.
	//all that we need to do is to implement the sorting algorithms...

	class safeArray_selectSort : public safeArray
	{
		void sort(unsigned long nElts = 0);
		void printSortMethod();
	};

	class safeArray_mergeSort : public safeArray
	{
		void sort(unsigned long nElts = 0);
		void printSortMethod();
	};

	class safeArray_quickSort : public safeArray
	{
		void sort(unsigned long nElts = 0);
		void printSortMethod();
	};

	//utility functions:
	void switch2(val_type* A, unsigned long i, unsigned long j);
}