#include "dllist.h"
#include <iostream>
using std::cout;

int main(void)
{
	dllist L;
	for (int i = 0; i < 5; i++) {
		L.insertAtFront(i);
	}
	L.print();
	L.printReverse();
	return 0;
}
