#include "stack.h"
#include <stddef.h> /* for size_t */
#include <cassert>

stack::stack() {
	/* have to establish the class invariant.
	 * class invariant:
	 * 1. data points to an array of allocated elements.
	 * 2. size is the size of the stack, and data[0...size-1]
	 *    are the contents of the stack.
	 * 3. i < j < size implies that data[i] has been in the
	 *    stack longer than data[j].
	 * */
	this->allocated = 5; /* NOTE: 5 is pretty arbitrary */
	this->data = new int[allocated];
	this->size = 0;
}

#ifdef USECOPYCONST
stack::stack(const stack& S)
{
	// this->data = S.data;  /* no good!! */
	this->allocated = S.allocated;
	this->data = new int[this->allocated];
	this->size = S.size;
	for (size_t i = 0; i < this->size; i++) {
		this->data[i] = S.data[i];
	}
}
#endif

stack& stack::operator=(const stack& RHS) {
	/* very similar to copy constructor, except you have
	 * to deal with the fact that the LHS already exists. */
	/* careful for self assignment: S = S; */
	if (&RHS == this) return *this;
	delete [] this->data;
	this->allocated = RHS.allocated;
	this->data = new int[this->allocated];
	this->size = RHS.size;
	for (size_t i = 0; i < this->size; i++) {
		this->data[i] = RHS.data[i];
	}
	return *this;
}

stack::~stack() {
	delete[] data;
}

void stack::push(int x) {
	if (this->size == this->allocated) /* out of space... */ {
		int* newdata = new int[this->size*2];
		for (size_t i = 0; i < this->size; i++) {
			newdata[i] = this->data[i];
		}
		delete [] this->data;
		this->data = newdata;
		this->allocated = this->size*2;
	}
	this->data[size++] = x;
	/* DONE: finish the analysis we started in class, and prove that
	 * n consecutive calls to this function will only cost O(n) time. */
	//O(1)*5 + O(5) + O(1)*5*2 + O(5*2)+...O(1)*5*2^i + O(5*2^i) for(i=0; i<=(lg(n)/5); i++)
	//The last term is O(n)
}
int stack::pop() {
	assert(this->size != 0);
	/* NOTE: assert takes a boolean, and will exit(1) with
	 * an error message if the boolean is false.  However, this
	 * only happens when no NDEBUG preprocessor symbol is defined.
	 * If NDEBUG is defined, assert is removed at the preporcessing
	 * stage.
	 * */
	return this->data[--size];
}
int stack::top() {
	assert(this->size != 0);
	return this->data[size-1];
}
