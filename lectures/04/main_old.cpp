/* test program for stack. */
#include "stack.h"
#include <iostream>
using std::cout;

/* this is why you need a copy constructor. */
void fn1(stack S) {
	cout << "calling fn1\n";
}

/* this is why you need an assignment operator. */
void fn2(stack& S) {
	cout << "calling fn2\n";
	stack temp;
	temp = S;
}

int main()
{
	stack S1;
	fn1(S1);
	return 0;
}
