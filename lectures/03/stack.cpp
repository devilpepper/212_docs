#include "stack.h"
#include <stddef.h> /* for size_t */

stack::stack() {
	/* have to establish the class invariant.
	 * class invariant:
	 * 1. data points to an array of allocated elements.
	 * 2. size is the size of the stack, and data[0...size-1]
	 *    are the contents of the stack.
	 * 3. i < j < size implies that data[i] has been in the
	 *    stack longer than data[j].
	 * */
	this->allocated = 5; /* NOTE: 5 is pretty arbitrary */
	this->data = new int[allocated];
	this->size = 0;
}

stack::stack(const stack& S) :
	size(S.size), allocated(S.allocated)
{
	// this->data = S.data;  /* no good!! */
	/* the above might look sensible, but it goes horribly wrong x_x
	 * TODO: find out why.  Hint: try to pass a by value
	 * stack parameter to a function. */
	//stack::data is a pointer to dynamically allocated data.
	//Since it's a pointer, assigning it's value to another pointer
	//will make both pointers point to the same object...
	this->allocated = S.allocated;
	this->data = new int[this->allocated];
	this->size = S.size;
	for (size_t i = 0; i < this->size; i++) {
		this->data[i] = S.data[i];
	}
}

stack::~stack() {
	delete[] data;
}

/* DONE: try to write the assignment operator. */
stack::stack& operator=(const stack& RHS)
{
	if(&RHS != this)
	{
		delete this->data;
		this->allocated = RHS.allocated;
		this->data = new int[this->allocated];
		this->size = RHS.size;
		for (size_t i = 0; i < this->size; i++) {
			this->data[i] = RHS.data[i];
		}
	}
	return *this;
}
/* DONE: try to write down push, pop, and top */
void stack::push(int x)
{
	if(this->size == this->allocated)
	{
		int *old = this->data;
		this->allocated *= 2;
		this->data = new int[allocated];

		for(size_t i = 0; i < this->size; i++) this->data[i] = old[i];
		delete old;
	}
	this->data[this->size++] = x;
}

int stack::pop()
{
	assert(this->size > 0);
	return this->data[--this->size];
}

int stack::top()
{
	assert(this->size > 0);
	return this->data[this->size - 1];
}
