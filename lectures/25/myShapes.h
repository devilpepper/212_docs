//the prototypes for all of our shapes...
#pragma once

#include "shape.h"

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

class Square : public Shape // this means that Square IS A Shape
{
public:
	Square(double sideLen = 1) {
		s = sideLen;
	}
	void printName() {
		cout << "square";
	}
	double area() {
		return s*s;
	}
	double perimeter() {
		return 4*s;
	}
	void printSymmetryGroup() {
		cout << "D_4";
	}
	unsigned long nSymmetries() {
		return 8;
	}

private:
	double s; //length of a side
};

class Circle : public Shape
{
public:
	Circle(double radius = 1) {
		r = radius;
	}
	void printName() {
		cout << "circle";
	}
	double area() {
		return 3.141592653589793 * r*r;
	}
	double perimeter() {
		return 2 * 3.141592653589793 * r;
	}
	void printSymmetryGroup() {
		cout << "D_infinity";
	}
	unsigned long nSymmetries() {
		return -1; // infinite...
	}
private:
	double r;
};

//this will be a regular triangle for now...
class Triangle : public Shape
{
public:
	Triangle(double sideLen = 1) {
		s = sideLen;
	}
	void printName() {
		cout << "triangle";
	}
	double area() {
		//1/2 * base * height == s^2 * (3^.5)/4
		return s * s * 0.433012701892219;
	}
	double perimeter() {
		return 3*s;
	}
	void printSymmetryGroup() {
		cout << "D_3";
	}
	unsigned long nSymmetries() {
		return 6;
	}
private:
	double s;
};

class Rectangle : public Shape
{
public:
	Rectangle(double length = 1,double width = 2) {
		l = length;
		w = width;
	}
	void printName() {
		cout << "rectangle";
	}
	double area() {
		//1/2 * base * height == s^2 * (3^.5)/4
		return l*w;
	}
	double perimeter() {
		return 2*(l+w);
	}
	void printSymmetryGroup() {
		if(l == w)
			cout << "D_4";
		else
			cout << "Z_2 x Z_2";
	}
	unsigned long nSymmetries() {
		if(l == w)
			return 8;
		else
			return 4;
	}

private:
	double l;
	double w;
};

/* TODO: write a new shape class, perhaps for a semicircle, and
 * plug it into the system. */
