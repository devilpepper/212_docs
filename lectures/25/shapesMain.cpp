// here is our main program, which we'll write SOLELY based
// on the abstract shape interface and associated functions.

#include "shape.h" // only the abstract interface is included.
// this is IMPORTANT!  The *only* file that we needed to know about
// to write this main program was shape.h which includes nothing
// more than an abstract interface for the shape class.  We could
// have written this program *without knowing anything about the details*.

#include <cstdio>

#include <iostream> //input output to console window...
using std::cin;
using std::cout;
using std::endl;

typedef std::list<std::string> strList;

int main(void)
{
	Shape** shapes;
	strList shapeList;
	getShapeTypes(shapeList);
	unsigned long i = 0;
	unsigned long nShapes = (unsigned long)shapeList.size();
	shapes = new Shape*[nShapes];

	for(strList::iterator li = shapeList.begin(); li != shapeList.end(); li++)
		shapes[i++] = createShape(*li); //*li is a string, e.g. "circle"

	for(i=0; i<nShapes; i++)
	{
		cout << "shape " << i << " is a ";
		shapes[i]->printName(); //polymorphism ("many shapes")
		cout << ":\n\tarea == " << shapes[i]->area() <<
			"\n\tperimeter == " << shapes[i]->perimeter() <<
			"\n\t#symmetries == " << shapes[i]->nSymmetries() <<
			endl << endl;
	}

	for(i=0; i<nShapes; i++)
		delete shapes[i];
	delete[] shapes;

	return 0;
}
