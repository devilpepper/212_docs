#include <iostream>
using std::cin;
using std::cout;

#if 1
class A {
public:
	void nv() {cout << "calling A::nv()\n";} /* non-virtual */
	virtual void v(){cout << "calling A::v()\n";} /* virtual */
};

#endif
class B : public A {
public:
	void nv() {cout << "calling B::nv()\n";}
	void v(){cout << "calling B::v()\n";}
};

class C : public B {
public:
	void nv() {cout << "calling C::nv()\n";}
	void v(){cout << "calling C::v()\n";}
};

int main(void)
{
#if 1
	A* p = new C();
#else
	B* p = new C();
#endif
	p->nv();
	p->v();
	return 0;
}

