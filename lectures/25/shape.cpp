#include "shape.h" //the abstract interface

#include "myShapes.h"
#include <cstdio>

#include <iostream>
using std::cout;
using std::endl;

void getShapeTypes(std::list<std::string>& L)
{
	L.push_back("square");
	L.push_back("circle");
	L.push_back("triangle");
	L.push_back("rectangle");
	return;
}

Shape* createShape(std::string& sName)
{
	Shape* pShape = 0; //pointer to a shape.
	if(sName == "square")
		pShape = new Square();
	else if(sName == "triangle")
		pShape = new Triangle();
	else if(sName == "circle")
		pShape = new Circle();
	else if(sName == "rectangle")
		pShape = new Rectangle();
	else
		fprintf(stderr, "Error!  Shape %s not found.\n",sName.c_str());

	return pShape;
}
