//the shape class will be an abstract class.  It is an organizational
//tool that lets us write programs at a level of abstraction closer
//to the way we think.
#pragma once

#include <list>
using std::list;
#include <string>
using std::string;

class Shape
{
public:
	virtual ~Shape() {}
	virtual void printName() = 0;
	/* NOTE: the virtual ... = 0 stuff means that I will NEVER
	 * implement this function for the shape class.  Such classes
	 * are called "abstract".  You cannot create instances of shape.
	 * I.e., shape x; // <-- this will not compile.  Well, you can't
	 * declare shapes, but you *can* declare shape pointers!  */
	virtual double area() = 0;
	virtual double perimeter() = 0;
	virtual void printSymmetryGroup() = 0;
	virtual unsigned long nSymmetries() = 0;
};

/* this function will fill the list with available shapes. */
void getShapeTypes(std::list<std::string>& L);

/* given a shape name, this function will find a match and create it
 * returning a pointer to the newly allocated shape. */
Shape* createShape(std::string& sName);
