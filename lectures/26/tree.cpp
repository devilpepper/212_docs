tree::tree() {
	root = new emptyNode;
}

void tree::insert(int x, abstractNode*& p) {
	root->insert(x, root);
}
bool nonemptyNode::insert (int x, abstractNode*& p) {
	if (x < this->data) return this->left->insert(x, this->left);
	if (x > this->data) return this->right->insert(x, this->right);
	return false;
}
bool emptyNode::insert(int x, abstractNode*& p) {
	/* IDEA: replace this empty node with a new nonempty one
	 * that contains x. */
	delete this;
	p = new nonemptyNode(x);
}

bool tree::search (int x) {
	return root->search(x);
}
bool emptyNode::search(int x) {
	return false;
}
bool nonemptyNode::search(int x) {
	if (x < this->data) return this->left->search(x);
	if (x > this->data) return this->right->search(x);
	return true;
}
