class abstractNode {
	public:
		virtual void insert(int x, abstractNode*& p) = 0;
		virtual void search(int x) = 0;
};

class emptyNode : public abstractNode {
	public:
	bool insert(int x, abstractNode*& p);
	bool search(int x);
};

class nonemptyNode : public abstractNode {
	public:
	nonemptyNode(int x = 0) : data(x), left(new emptyNode()), right(new emptyNode()) {};
	bool insert (int x, abstractNode*& p);
	bool search(int x);
	private:
	int data;
	abstractNode* left;
	abstractNode* right;
};

class tree {
	public:
		tree();
		tree(const tree& T);
		~tree();
		tree& operator=(const tree& T);
		bool insert(int x);
		bool search(int x);
	private:
		abstractNode* root;
};
