#include <cstdio>

/* templates.  think of them as "blueprints" for
 * other datatypes, or for functions. */
template <typename T>
void swap(T& a,T& b) {
	T temp = a;
	a = b;
	b = temp;
}

/* TODO: write a template function called 'findMax' which
 * takes an array of T, and returns the largest element in
 * the array.  Call this function on an array of integers.
 * Then define a struct like this: */
struct nonsense {
	int a;
	double b;
};
/* and call findMax on an array of nonsense.  Try to understand
 * the errors you get. */

/* you can also template classes and structs: */
#if 0
template <typename T>
class vector {
public:
	/* member functions... */
	void push_back(T x);
private:
	T* data;
};
#endif

/* TODO: compile with the following, and compare the
 * assembly output (the .s files):
 * g++ -S -o none.s templates.cpp
 * g++ -S -o ints.s -DINTS templates.cpp
 * g++ -S -o chars.s -DCHARS templates.cpp
 * g++ -S -o both.s -DCHARS -DINTS templates.cpp
 * */

int main()
{
#ifdef INTS
	int x = 99; int y = 2;
	printf("x = %i; y = %i.\n",x,y);
	swap(x,y);
	printf("x = %i; y = %i.\n",x,y);
#endif
#ifdef CHARS
	/* now with characters: */
	char a = 'h'; char b = 'i';
	printf("a = %c; b = %c.\n",a,b);
	swap(a,b);
	printf("a = %c; b = %c.\n",a,b);
#endif
	return 0;
}
